Source: python-htmltmpl
Section: python
Priority: optional
Maintainer: Sergio Talens-Oliag <sto@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: cdbs (>= 0.4.42), debhelper (>= 5.0.37.1)
Build-Depends-Indep: python-all-dev, python-support (>= 0.2.3)
XS-Python-Version: all
Vcs-Git: https://salsa.debian.org/python-team/packages/python-htmltmpl.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-htmltmpl
Standards-Version: 3.7.2.0
Homepage: http://htmltmpl.sourceforge.net

Package: python-htmltmpl
Architecture: all
Depends: ${python:Depends}
XB-Python-Version: ${python:Versions}
Description: Templating engine for separation of code and HTML
 The purpose of the templating engine is to provide web application
 developers, who need to separate program code and design (HTML code)
 of their web application projects, with a templating tool that can be
 easily used by cooperating webdesigners who have no programming
 skills.
 .
 Templating language provided by the engine is inspired by Perl
 templating module HTML::Template. Templates created for HTML::Template
 can be used with this engine in case they do not violate character case
 rules of htmltmpl.
 .
 The engine is currently available for Python and PHP. The Python
 package includes easydoc, a module which uses the templating engine to
 generate HTML documentation from docstrings embedded in source files
 of Python modules.
